# **Running guide**

Install pipenv. You cat do it via pip.

Clone repo:

`$ git clone git@gitlab.com:ElbrusGasanov/cinema_box.git`


Install requirements:

`$ pipenv install -d --pre`

Activate virtual environment:

`$ pipenv shell`


Install pre-commit hooks:

`$ pre-commit install`


Initiate the database:

`$ cd src && python manage.py migrate`


Run server:

`$ python manage.py runserver`

