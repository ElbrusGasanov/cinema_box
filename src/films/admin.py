from django.contrib import admin

from films.models import Director, Film


class DirectorModelAdmin(admin.ModelAdmin):
    list_display = (
        "first_name",
        "last_name",
    )


class FilmModelAdmin(admin.ModelAdmin):
    list_display = (
        "director",
        "title",
        "description",
        "get_age",
    )

    def get_age(self, film):
        return f"{film.age}+"

    get_age.short_description = "Age"  # type: ignore


admin.site.register(Director, DirectorModelAdmin)
admin.site.register(Film, FilmModelAdmin)
