from django.db import models


class Director(models.Model):
    first_name = models.CharField(verbose_name="First name", max_length=32)
    last_name = models.CharField(
        verbose_name="Last name", max_length=32, blank=True, null=True
    )

    def __str__(self):
        return (
            f"{self.first_name} {self.last_name}" if self.last_name else self.first_name
        )


class Film(models.Model):
    director = models.ForeignKey(
        verbose_name="Director", to=Director, on_delete=models.PROTECT
    )

    title = models.CharField(verbose_name="Title", max_length=64)
    description = models.TextField(verbose_name="Description")
    age = models.PositiveSmallIntegerField(verbose_name="Age", blank=True, default=0)
