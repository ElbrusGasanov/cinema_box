from django.urls import path

from api.v1.views import DirectorListAPIView, FilmListAPIView

urlpatterns = [
    path("films/", FilmListAPIView.as_view()),
    path("directors/", DirectorListAPIView.as_view()),
]
