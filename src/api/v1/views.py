from rest_framework.generics import ListAPIView

from api.v1.serializers import DirectorSerializer, FilmSerializer
from films.models import Director, Film


class FilmListAPIView(ListAPIView):
    serializer_class = FilmSerializer
    queryset = Film.objects.all()


class DirectorListAPIView(ListAPIView):
    serializer_class = DirectorSerializer
    queryset = Director.objects.all()
